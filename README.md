# pipo_go

## example `pipo.toml`

```toml
[irc_chatjunkies]
service_type = "irc"
server = "irc.chatjunkies.org"
port = "6697"
nick = "joe_schmo_420"
channels = [
  { name = "#redbook",   group = "one" },
  { name = "#gigachads", group = "two" }
]

["matrix_matrix.example.xyz"]
service_type = "matrix"
sender = "pipo_go"
server = "matrix.example.xyz"
access_token = "123xyz"
port = "5000"
rooms = [
  { room_id = "!gfulekzRaAyKmlLYCk:matrix.example.xyz", group = "one" },
  { room_id = "!pjLqrdFwrnboaGtGIq:matrix.example.xyz", group = "two" }
]

[discord_example]
service_type = "discord"
auth_token = "123xyz"
channels = [
  { channel_id = "1212221787056898058", group = "one" },
  { channel_id = "1071757763119689818", group = "two" }
]

[slack_example]
service_type = "slack"
sender_id = "B06N6V4NKQQ"
signing_secret = "123xyz"
auth_token = "456abc"
port = "8009"
channels = [
  { channel_id = "C06MLBF5SE7", group = "one" },
  { channel_id = "CS1Q8KBUN",   group = "two" }
]
```

