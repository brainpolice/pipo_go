module pipo_go

go 1.22.0

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/bwmarrin/discordgo v0.27.1
	github.com/go-chi/chi/v5 v5.0.12
	github.com/jmoiron/sqlx v1.3.5
	github.com/mattn/go-sqlite3 v1.14.22
	github.com/slack-go/slack v0.12.5
	gopkg.in/sorcix/irc.v2 v2.0.0-20200812151606-3f15758ea8c7
)

require (
	github.com/gorilla/websocket v1.5.1 // indirect
	golang.org/x/crypto v0.20.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
)
