package main

import (
	"context"
	"flag"
	"io"
	"log"
	"os"
	"os/signal"
	"pipo_go/lib"
)

func main() {
	if err := mainErr(); err != nil {
		log.Fatal(err)
	}
}

func mainErr() error {
	flag.Parse()

	f, err := os.OpenFile(*lib.FlagLogPath, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	defer f.Close()

	log.SetOutput(io.MultiWriter(os.Stdout, f, lib.SockLogger{}))

	//

	cfgPath, err := lib.ResolveCfgPath()
	if err != nil {
		return err
	}

	cfgBytes, err := os.ReadFile(cfgPath)
	if err != nil {
		return err
	}

	services, err := lib.ParseCfg(cfgBytes)
	if err != nil {
		return err
	}

	//

	Pipo := lib.NewPipo(services)
	defer func() {
		log.Printf("info waiting for pipo to finish")
		<-Pipo.Closed
	}()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go Pipo.Run(ctx)

	var sigc = make(chan os.Signal, 1)
	signal.Notify(sigc, os.Interrupt, os.Kill)

	select {
	case <-sigc:
		return nil
	}
}
