package main

import (
	"context"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"os/signal"
	"pipo_go/lib"
	"time"

	"github.com/go-chi/chi/v5"
)

func main() {
	flag.Parse()
	if len(*lib.FlagToken) < 1 {
		log.Fatal(errors.New("no token"))
	}

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	go readSocket(ctx)

	if err := mainErr(ctx); err != nil {
		log.Fatal(err)
	}
}

func mainErr(ctx context.Context) error {
	ctx, cancel := context.WithCancel(ctx)

	eventc := make(chan any)
	fin := make(chan struct{})
	go ciLoop(ctx, eventc, fin)
	defer func() {
		cancel()
		<-fin
	}()

	r := chi.NewRouter()
	r.Post("/", func(w http.ResponseWriter, r *http.Request) {
		if header := r.Header.Get("X-Gitlab-Token"); header != *lib.FlagToken {
			http.Error(w, "", http.StatusUnauthorized)
			return
		}

		if header := r.Header.Get("X-Gitlab-Event"); header != "Push Hook" {
			http.Error(w, "", http.StatusBadRequest)
			return
		}

		d := json.NewDecoder(r.Body)
		var event any
		if err := d.Decode(&event); err != nil {
			http.Error(w, "", http.StatusInternalServerError)
			return
		}

		// log.Printf("%+v", event)
		eventc <- event
	})

	s := http.Server{
		Addr:    *lib.FlagAddrHttp,
		Handler: r,
	}

	go func() {
		var sigc = make(chan os.Signal, 1)
		signal.Notify(sigc, os.Interrupt, os.Kill)
		<-sigc
		s.Shutdown(ctx)
	}()

	if err := s.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
		return err
	}

	return nil
}

////

func readSocket(ctx context.Context) {
	listener, err := net.Listen("tcp", *lib.FlagAddrSock)
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()

	sockc := make(chan net.Conn)
	errc := make(chan error)
	for {
		go func() {
			sock, err := listener.Accept()
			if err != nil {
				errc <- err
				return
			}
			sockc <- sock
		}()

		select {
		case <-ctx.Done():
			return

		case err := <-errc:
			log.Print(err)
			continue

		case sock := <-sockc:
			go func() {
				defer sock.Close()
				b := make([]byte, 1024)
				mLen, err := sock.Read(b)
				if err != nil {
					log.Println(err)
					return
				}
				fmt.Printf("pipo | %s", b[:mLen])
			}()
		}
	}
}

func ciLoop(ctx context.Context, eventc chan any, fin chan struct{}) {
	defer func() {
		fin <- struct{}{}
	}()

	for {
		ci(ctx, eventc)

		select {
		case <-ctx.Done():
			return

		default:
			fmt.Println("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$")
			log.Print("restart ci")
			continue
		}
	}
}

func ci(ctx context.Context, eventc chan any) {
	errc := make(chan error)

	////

	cmd := exec.CommandContext(ctx, "pgrep", "-x", "pipo")
	fmt.Println("********************************************************************************")
	log.Print(cmd.Args)
	o, err := cmd.Output()
	if err == nil {
		err = errors.New("already running pipo")
		goto Waiting
	}

	cmd = exec.CommandContext(ctx, "git", "pull")
	fmt.Println("********************************************************************************")
	log.Print(cmd.Args)
	o, err = cmd.Output()
	if err != nil {
		goto Waiting
	}
	log.Print(string(o))

	cmd = exec.CommandContext(ctx, "make", "pipo")
	fmt.Println("********************************************************************************")
	log.Print(cmd.Args)
	o, err = cmd.Output()
	if err != nil {
		goto Waiting
	}
	log.Print(string(o))

	cmd = exec.CommandContext(ctx, "./bin/pipo")
	fmt.Println("********************************************************************************")
	log.Print(cmd.Args)
	err = cmd.Start()

	////

	defer func() {
		if err != nil {
			return
		}

		fmt.Println("================================================================================")
		log.Print("sending interrupt")

		cmd.Process.Signal(os.Interrupt)

		errc := make(chan error)
		go func() {
			errc <- cmd.Wait()
			log.Printf("piiipooooo!!!")
		}()

		select {
		case <-time.After(5 * time.Second):
			log.Print("stop timed out")
		case err := <-errc:
			if err != nil {
				log.Print(err)
			}
		}
	}()

Waiting:
	if err != nil {
		fmt.Println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
		log.Print(err)
        err = nil
	}

	fmt.Println("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
	select {
	case <-ctx.Done():
		return
	case <-eventc:
		return
	case err := <-errc:
		log.Print(err)
		goto Waiting
	}
}
