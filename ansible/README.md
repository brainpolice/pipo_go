## example `inventory.yml`

```yaml
---
local:
  hosts:
    localhost:
      ansible_connection: local
      ssh0:
        from:
          hostname: localhost
          user: example
        to:
          user: pipo
          groups: docker

remote:
  hosts:
    "35.243.xxx.251":
      synapse_pipo:
        server_name: matrix.example.xyz
        registration:
          - name: pipo_go
            content: |
              id: "pipo_go"
              url: "http://35.243.xxx.251:5000"
              as_token: example
              hs_token: exmaple
              sender_localpart: pipo_go
              namespaces:
                users: []
                aliases: []
                rooms:
                  - exclusive: false
                    regex: "!pjLqrdFwrnboaGtGIq.*"
                  - exclusive: false
                    regex: "!gfulekzRaAyKmlLYCk.*"

      nginx_certbot_synapse:
        server_name: "matrix.example.xyz"
        certbot_email: "example.example@gmail.com"

      nginx_synapse:
        server_name: matrix.example.xyz
        server: |
          server {
              server_name matrix.example.xyz;

              listen 443 ssl http2;
              listen 8448 ssl http2 default_server;

              ssl_certificate         /root/nginx_certbot_matrix.example.xyz/certbot/conf/live/matrix.example.xyz/fullchain.pem;
              ssl_certificate_key     /root/nginx_certbot_matrix.example.xyz/certbot/conf/live/matrix.example.xyz/privkey.pem;
              ssl_dhparam             /root/nginx_certbot_matrix.example.xyz/dhparam/dhparam-2048.pem;

              ssl_buffer_size 8k;
              ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
              ssl_prefer_server_ciphers on;
              ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

              location ~ ^(/_matrix|/_synapse/client) {
                  proxy_pass http://127.0.0.1:8008;
                  proxy_set_header X-Forwarded-For $remote_addr;
                  proxy_set_header X-Forwarded-Proto $scheme;
                  proxy_set_header Host $host;
                  client_max_body_size 50M;
                  proxy_http_version 1.1;
              }
          }

      nginx_certbot_piposlack:
        server_name: "piposlack.example.xyz"
        certbot_email: "example.example@gmail.com"

      nginx_piposlack:
        server_name: piposlack.example.xyz
        server: |
          server {
              server_name piposlack.example.xyz;

              listen 443 ssl http2;
              listen 8448 ssl http2;

              ssl_certificate         /root/nginx_certbot_piposlack.example.xyz/certbot/conf/live/piposlack.example.xyz/fullchain.pem;
              ssl_certificate_key     /root/nginx_certbot_piposlack.example.xyz/certbot/conf/live/piposlack.example.xyz/privkey.pem;
              ssl_dhparam             /root/nginx_certbot_piposlack.example.xyz/dhparam/dhparam-2048.pem;

              ssl_buffer_size 8k;
              ssl_protocols TLSv1.2 TLSv1.1 TLSv1;
              ssl_prefer_server_ciphers on;
              ssl_ciphers ECDH+AESGCM:ECDH+AES256:ECDH+AES128:DH+3DES:!ADH:!AECDH:!MD5;

              location / {
                  proxy_pass http://127.0.0.1:8009;
                  proxy_set_header X-Forwarded-For $remote_addr;
                  proxy_set_header X-Forwarded-Proto $scheme;
                  proxy_set_header Host $host;
                  client_max_body_size 50M;
                  proxy_http_version 1.1;
              }
          }
```
