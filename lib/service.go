package lib

import (
	"context"
	"encoding/json"
	"errors"
	"log"
	"os"
	"time"

	"github.com/BurntSushi/toml"
)

type room interface {
	id() string
	group() string
}

type service interface {
	fin() <-chan struct{}
	recv() chan<- pipoEvent
    init(id string)
	run(ctx context.Context)
}

var retryTimeout = 10 * time.Second

func ResolveCfgPath() (cfgPath string, err error) {
	home, ok := os.LookupEnv("XDG_CONFIG_HOME")
	c := home + "/pipo/config.toml"

	if _, err = os.Stat(*FlagCfgPath); err == nil {
		cfgPath = *FlagCfgPath
	} else if _, err = os.Stat(c); ok && err == nil {
		cfgPath = c
	} else if _, err = os.Stat("/etc/pipo.toml"); err == nil {
		cfgPath = "/etc/pipo.toml"
	} else {
		err = errors.New("no configuration file")
	}

	if err == nil {
		log.Printf("info cfg path: %s", cfgPath)
	}

	return
}

func ParseCfg(b []byte) (*map[string]service, error) {
	s := make(map[string]service)

	var root map[string]interface{}
	if err := toml.Unmarshal(b, &root); err != nil {
		return nil, err
	}

	for serviceId, service := range root {
		serviceb, err := json.Marshal(service)
		if err != nil {
			return nil, err
		}

		service_type := service.(map[string]interface{})["service_type"].(string)
		switch service_type {
		case "irc":
			var irc ircService
			if err := json.Unmarshal(serviceb, &irc); err != nil {
				return nil, err
			}
			s[serviceId] = &irc

		case "matrix":
			var matrix matrixService
			if err := json.Unmarshal(serviceb, &matrix); err != nil {
				return nil, err
			}
			s[serviceId] = &matrix

		case "discord":
			var discord discordService
			if err := json.Unmarshal(serviceb, &discord); err != nil {
				log.Fatalf("%v", err)
			}
			s[serviceId] = &discord

		case "slack":
			var slack slackService
			if err := json.Unmarshal(serviceb, &slack); err != nil {
				log.Fatalf("%v", err)
			}
			s[serviceId] = &slack

		default:
			continue
		}
	}

	return &s, nil
}
