package lib

import (
	"context"
	"crypto/tls"
	"fmt"
	"log"
	"strings"
	"time"

	irc "gopkg.in/sorcix/irc.v2"
)

type ircChannel struct {
	Name  string
	Group string

	service *ircService
}

func (x *ircChannel) id() string {
	return x.service.id + "_" + x.Name
}

func (x *ircChannel) group() string {
	return x.Group
}

//

type ircService struct {
	ServiceType string `json:"service_type" toml:"service_type"`
	Server      string
	Port        string
	Nick        string
	Channels    []*ircChannel

	id    string
	finc  chan struct{}
	recvc chan pipoEvent

	client   *irc.Conn
	messagec chan *irc.Message
	errc     chan error
}

func (x *ircService) recv() chan<- pipoEvent {
	return x.recvc
}

func (x *ircService) fin() <-chan struct{} {
	return x.finc
}

func (x *ircService) init(id string) {
	x.id = id
	x.finc = make(chan struct{})
	x.recvc = make(chan pipoEvent)
	x.errc = make(chan error)
	for _, channel := range x.Channels {
		channel.service = x
	}
}

func (x *ircService) run(ctx context.Context) {
	defer func() { x.finc <- struct{}{} }()

	log.Printf("info starting %s", x.id)
	for err := x.runErr(ctx); err != nil; {
		log.Printf("info %s retrying in %d seconds %v", x.id, int(retryTimeout.Seconds()), err)
		<-time.After(retryTimeout)
	}
}

func (x *ircService) decode(ctx context.Context) {
	messagec := make(chan *irc.Message)
	for {
		go func() {
			message, err := x.client.Decode()
			if err != nil {
				x.errc <- err
				return
			}
			messagec <- message
		}()

		select {
		case <-ctx.Done():
			return
		case message := <-messagec:
			x.messagec <- message
		}
	}
}

func (x *ircService) runErr(ctx context.Context) error {
	client := make(chan *irc.Conn)
	go func() {
		cl, err := irc.DialTLS(fmt.Sprintf("%s:%s", x.Server, x.Port), &tls.Config{
			ServerName: x.Server,
		})
		if err != nil {
			x.errc <- err
			return
		}
		client <- cl
	}()

	select {
	case <-ctx.Done():
		return nil
	case err := <-x.errc:
		return err
	case x.client = <-client:
	}

	defer x.client.Close()

	decodeCtx, cancelDecodeCtx := context.WithCancel(context.Background())
	defer cancelDecodeCtx()
	x.messagec = make(chan *irc.Message)
	go x.decode(decodeCtx)

	for {
		select {
		case <-ctx.Done():
			if err := x.client.Encode(&irc.Message{
				Command: "QUIT",
				Params:  []string{},
			}); err != nil {
				log.Printf("info %s problem exiting cleanly %v", x.id, err)
				return nil
			}

			timeout, cancelTimeout := context.WithTimeout(context.Background(), 8*time.Second)
			defer cancelTimeout()
			for {
				select {
				case <-timeout.Done():
					log.Printf("info %s clean exit timed out", x.id)
					return nil
				case message := <-x.messagec:
					if message.Command == "ERROR" && strings.Contains(message.Param(0), "[Client exited]") {
						return nil
					}
				}
			}

		case err := <-x.errc:
			return err

		case event := <-x.recvc:
			for _, channel := range x.Channels {
				if channel.id() == event.room.id() {
					continue
				}
				if channel.Group != event.room.group() {
					continue
				}
				if err := x.client.Encode(&irc.Message{
					Command: "PRIVMSG",
					Params:  []string{channel.Name, event.formatted()},
				}); err != nil {
					return err
				}
			}

		case message := <-x.messagec:
			log.Printf("info %s %s", x.id, message)

			switch {
			case message.Command == "NOTICE" && strings.Contains(message.Param(1), "Found your hostname"):
				if err := x.client.Encode(&irc.Message{
					Command: "NICK",
					Params:  []string{x.Nick},
				}); err != nil {
					return err
				}
				if err := x.client.Encode(&irc.Message{
					Command: "USER",
					Params:  []string{fmt.Sprintf("%s * *", x.Nick), x.Nick},
				}); err != nil {
					return err
				}

			case message.Command == "NOTICE" && strings.Contains(message.Param(1), "You are connected"):
				for _, channel := range x.Channels {
					if err := x.client.Encode(&irc.Message{
						Command: "JOIN",
						Params:  []string{channel.Name},
					}); err != nil {
						return err
					}
				}

			case message.Command == "PING":
				log.Printf("info %s trying to send pong", x.id)
				if err := x.client.Encode(&irc.Message{
					Command: "PONG",
					Params:  []string{message.Param(0)},
				}); err != nil {
					return err
				}

			case message.Command == "PRIVMSG":
				for _, channel := range x.Channels {
					if channel.Name != message.Param(0) {
						continue
					}
					ctxPipoRecv(ctx) <- pipoEvent{
						room:    channel,
						from:    message.Prefix.Name,
						via:     channel.Name,
						on:      x.Server,
						message: message.Param(1),
					}
				}
			}
		}
	}
}
