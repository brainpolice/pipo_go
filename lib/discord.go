package lib

import (
	"context"
	"log"
	"time"

	"github.com/bwmarrin/discordgo"
)

type discordChannel struct {
	ChannelId string `json:"channel_id" toml:"channel_id"`
	Group     string

	service *discordService
}

func (x *discordChannel) id() string {
	return x.service.id + "_" + x.ChannelId
}

func (x *discordChannel) group() string {
	return x.Group
}

func (x *discordChannel) sync(ctx context.Context) {
	c := pipoDictRow{Key: x.ChannelId + ":name"}
	go c.sync(ctx, func() (string, error) {
		channel, err := x.service.discordSession.Channel(x.ChannelId)
		if err != nil {
			return "", err
		}
		return channel.Name, nil
	})

	g := pipoDictRow{Key: x.ChannelId + ":guild_name"}
	go g.sync(ctx, func() (string, error) {
		channel, err := x.service.discordSession.Channel(x.ChannelId)
		if err != nil {
			return "", err
		}
		guild, err := x.service.discordSession.Guild(channel.GuildID)
		if err != nil {
			return "", err
		}
		return guild.Name, nil
	})
}

func (x *discordChannel) name(ctx context.Context) string {
	r := &pipoDictRow{Key: x.ChannelId + ":name"}
	r.get(ctx)
	return r.Value
}

func (x *discordChannel) guildName(ctx context.Context) string {
	r := &pipoDictRow{Key: x.ChannelId + ":guild_name"}
	r.get(ctx)
	return r.Value
}

type discordService struct {
	ServiceType string `json:"service_type" toml:"service_type"`
	AuthToken   string `json:"auth_token" toml:"auth_token"`
	Channels    []*discordChannel

	id    string
	finc  chan struct{}
	recvc chan pipoEvent

	discordSession *discordgo.Session
}

func (x *discordService) recv() chan<- pipoEvent {
	return x.recvc
}

func (x *discordService) fin() <-chan struct{} {
	return x.finc
}

func (x *discordService) init(id string) {
	x.id = id
	x.finc = make(chan struct{})
	x.recvc = make(chan pipoEvent)
	for _, channel := range x.Channels {
		channel.service = x
	}
}

func (x *discordService) run(ctx context.Context) {
	defer func() { x.finc <- struct{}{} }()

	log.Printf("info starting %s", x.id)
	for err := x.runErr(ctx); err != nil; {
		log.Printf("info %s retrying in %d seconds %v", x.id, int(retryTimeout.Seconds()), err)
		<-time.After(retryTimeout)
	}
}

func (x *discordService) runErr(ctx context.Context) error {
	var err error
	x.discordSession, err = discordgo.New("Bot " + x.AuthToken)
	if err != nil {
		return err
	}
	defer x.discordSession.Close()

	for _, c := range x.Channels {
		c.sync(ctx)
	}

	x.discordSession.AddHandler(func(s *discordgo.Session, m *discordgo.MessageCreate) {
		if m.Author.ID == s.State.User.ID {
			return
		}
		log.Printf("info %s %s", x.id, m.Content)

		for _, c := range x.Channels {
			if c.ChannelId != m.ChannelID {
				continue
			}
			ctxPipoRecv(ctx) <- pipoEvent{
				room:    c,
				from:    m.Author.Username,
				via:     c.name(ctx),
				on:      c.guildName(ctx) + " (Discord)",
				message: m.Content,
			}
		}
	})

	x.discordSession.Identify.Intents = discordgo.IntentGuildMessages
	if err := x.discordSession.Open(); err != nil {
		return err
	}

	for {
		select {
		case <-ctx.Done():
			return nil

		case event := <-x.recvc:
			for _, channel := range x.Channels {
				if channel.id() == event.room.id() {
					continue
				}
				if channel.Group != event.room.group() {
					continue
				}
				if _, err := x.discordSession.ChannelMessageSend(
					channel.ChannelId,
					event.formatted(),
				); err != nil {
					return err
				}
			}
		}
	}
}
