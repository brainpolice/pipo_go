package lib

import (
	"context"
	"log"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/mattn/go-sqlite3"
)

type pipoDictRow struct {
	Id        int
	Key       string `db:"k"`
	Value     string `db:"v"`
	Timestamp string `db:"ts"`
}

func (x *pipoDictRow) get(ctx context.Context) {
	var rows []pipoDictRow
	if err := Db.SelectContext(ctx, &rows,
		`SELECT *
        FROM dict
        WHERE k=$1`, x.Key,
	); err != nil {
		log.Fatalf("%v", err)
	}
	if len(rows) > 0 {
		*x = rows[0]
		return
	}
	x.Value = "<empty>"
}

var syncTimeout = 1 * time.Hour

func (x *pipoDictRow) sync(ctx context.Context, fetchFn func() (string, error)) {
	if err := x.syncErr(ctx, fetchFn); err != nil {
		if ctx.Err() != context.Canceled {
			log.Fatalf("%v", err)
		}
	}
}

func (x *pipoDictRow) syncErr(ctx context.Context, fetchFn func() (string, error)) error {
	for {
		var rows []pipoDictRow
		if err := Db.SelectContext(ctx, &rows,
			`SELECT *
			FROM dict
			WHERE k=$1`, x.Key,
		); err != nil {
			return err
		}

		switch {
		case len(rows) < 1:
			v, err := fetchFn()
			if err != nil {
				log.Printf("warning %v", err)
				goto Waiting
			}

			log.Printf("info inserting %s", x.Key)
			if _, err := Db.ExecContext(ctx,
				`INSERT INTO dict
				(k, v, ts)
				VALUES
				($1, $2, $3)`,
				x.Key, v, time.Now().Format(time.RFC3339),
			); err != nil {
				return err
			}

		default:
			row := rows[0]
			t1, err := time.Parse(time.RFC3339, row.Timestamp)
			if err != nil {
				return err
			}
			t2 := time.Now()
			td := t2.Sub(t1)

			// log.Printf("t1 %v", t1)
			// log.Printf("t2 %v", t2)
			// log.Printf("td %v", td)
			// log.Printf("td < syncTimeout %v", td < syncTimeout)

			if td < syncTimeout {
				select {
				case <-ctx.Done():
					return nil
				case <-time.After(syncTimeout - td):
				}
			}

			log.Printf("info fetching %s", x.Key)
			v, err := fetchFn()
			if err != nil {
				log.Printf("warning %v", err)
				goto Waiting
			}

			if _, err := Db.ExecContext(ctx,
				`UPDATE dict
				SET v=$1, ts=$2
				WHERE k=$3`,
				v, time.Now().Format(time.RFC3339), x.Key,
			); err != nil {
				return err
			}
		}

		continue

	Waiting:
		select {
		case <-ctx.Done():
			return nil
		case <-time.After(syncTimeout):
		}
	}
}

var Db *sqlx.DB

func init() {
	db, err := sqlx.Connect("sqlite3", "./db.sqlite")
	if err != nil {
		log.Fatalf("%v", err)
	}
	Db = db
}
