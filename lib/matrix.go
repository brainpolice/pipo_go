package lib

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/go-chi/chi/v5"
)

type matrixRoom struct {
	Group  string
	RoomId string `json:"room_id" toml:"room_id"`

	service *matrixService
}

func (x *matrixRoom) id() string {
	return x.service.id + "_" + x.RoomId
}

func (x *matrixRoom) group() string {
	return x.Group
}

func (x *matrixRoom) sync(ctx context.Context) {
    c := pipoDictRow{Key: x.RoomId + ":name"}
	go c.sync(ctx, func() (string, error) {
		url := fmt.Sprintf(
			"https://%s/_matrix/client/v3/rooms/%s/aliases?access_token=%s",
			x.service.Server, x.RoomId, x.service.AccessToken,
		)
		req, err := http.NewRequest(http.MethodGet, url, nil)
		if err != nil {
			log.Printf("%v", err)
			return "", err
		}

		resc := make(chan *http.Response)
		errc := make(chan error)
		go func() {
			res, err := x.service.client.Do(req)
			if err != nil {
				errc <- err
				return
			}
			resc <- res
		}()

		select {
		case <-ctx.Done():
			return "", nil

		case err := <-errc:
			return "", err

		case res := <-resc:
			if res.StatusCode != 200 {
				return "", errors.New("request failed")
			}
			d := json.NewDecoder(res.Body)
			var j struct{ Aliases []string }
			if err := d.Decode(&j); err != nil {
				return "", err
			}
			if len(j.Aliases) > 0 {
				return j.Aliases[0], nil
			}
			return "<empty>", nil
		}
	})
}

func (x *matrixRoom) alias(ctx context.Context) string {
	r := &pipoDictRow{Key: x.RoomId + ":name"}
	r.get(ctx)
	return r.Value
}

////

type matrixService struct {
	ServiceType string `json:"service_type" toml:"service_type"`
	Server      string
	Port        string
	Sender      string
	AccessToken string `json:"access_token" toml:"access_token"`
	Rooms       []*matrixRoom

	id    string
	finc  chan struct{}
	recvc chan pipoEvent

	client *http.Client
	router *chi.Mux
	server *http.Server
	errc   chan error
}

func (x *matrixService) recv() chan<- pipoEvent {
	return x.recvc
}

func (x *matrixService) fin() <-chan struct{} {
	return x.finc
}

func (x *matrixService) init(id string) {
	x.id = id
	x.finc = make(chan struct{})
	x.recvc = make(chan pipoEvent)
	x.client = &http.Client{}
	x.errc = make(chan error)
	for _, room := range x.Rooms {
		room.service = x
	}
}

type ClientEventContent struct {
	Body    string
	Msgtype string
}

type ClientEvent struct {
	Age      int64
	Content  ClientEventContent
	EventId  string `json:"event_id"`
	RoomId   string `json:"room_id"`
	Sender   string
	StateKey *string `json:"state_key"`
	Type     string
	UserId   string `json:"user_id"`
}

type events struct {
	Events []ClientEvent
}

func (x *matrixService) run(ctx context.Context) {
	defer func() { x.finc <- struct{}{} }()

	for _, room := range x.Rooms {
		room.sync(ctx)
	}

	x.router = chi.NewRouter()
	x.router.Put(
		"/_matrix/app/v1/transactions/{transaction}",
		func(w http.ResponseWriter, r *http.Request) {
			var evs events
			if err := json.NewDecoder(r.Body).Decode(&evs); err != nil {
				log.Printf("error %s json decode %v", x.id, err)
				http.Error(w, "error decode", http.StatusBadRequest)
				return
			}

			for _, ev := range evs.Events {
				if ev.StateKey != nil ||
					ev.Type != "m.room.message" ||
					ev.Content.Msgtype != "m.text" ||
					ev.UserId == fmt.Sprintf("@%s:%s", x.Sender, x.Server) {
					continue
				}

				log.Printf("info %s %s", x.id, ev.Content.Body)
				for _, room := range x.Rooms {
					if room.RoomId != ev.RoomId {
						continue
					}
					ctxPipoRecv(ctx) <- pipoEvent{
						room: room,
						// from:    ev.Sender,
						from:    strings.Split(ev.Sender, ":")[0],
						on:      room.alias(ctx),
						message: ev.Content.Body,
					}
				}
			}

			w.Write([]byte("{}"))
		},
	)

	log.Printf("info starting %s", x.id)
	for err := x.runErr(ctx); err != nil; {
		log.Printf("info %s retrying in %d seconds %v", x.id, int(retryTimeout.Seconds()), err)
		<-time.After(retryTimeout)
	}
}

func (x *matrixService) runErr(ctx context.Context) error {
	var wg sync.WaitGroup
	for _, room := range x.Rooms {
		url := fmt.Sprintf(
			"https://%s/_matrix/client/v3/rooms/%s/join?access_token=%s",
			x.Server, room.RoomId, x.AccessToken,
		)
		payload, err := json.Marshal(map[string]interface{}{"reason": "pipo directed 2 join"})
		if err != nil {
			return nil
		}

		req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
		req.Header.Set("Content-Type", "application/json")
		if err != nil {
			return nil
		}

		wg.Add(1)
		go func() {
			defer wg.Done()

			resc := make(chan *http.Response)
			go func() {
				res, err := x.client.Do(req)
				if err != nil {
					x.errc <- err
					return
				}
				resc <- res
			}()

			select {
			case <-ctx.Done():
				return
			case res := <-resc:
				defer res.Body.Close()
				if res.StatusCode != 200 {
					x.errc <- errors.New("request failed")
				}
			}
		}()
	}
	wg.Wait()

	// https://github.com/go-chi/chi/blob/master/_examples/graceful/main.go
	x.server = &http.Server{
		Addr:    fmt.Sprintf(":%s", x.Port),
		Handler: x.router,
	}

	listenErr := make(chan error, 1)
	go func() {
		listenErr <- x.server.ListenAndServe()
	}()
	defer func() {
		if err := x.server.Shutdown(ctx); err != nil {
			log.Print(err)
		}
		<-listenErr
	}()

	for {
		select {
		case <-ctx.Done():
			return nil

		case err := <-x.errc:
			return err

		case err := <-listenErr:
			listenErr <- err
			return err

		case event := <-x.recvc:
			for _, room := range x.Rooms {
				if room.id() == event.room.id() {
					continue
				}
				if room.Group != event.room.group() {
					continue
				}

				url := fmt.Sprintf(
					"https://%s/_matrix/client/v3/rooms/%s/send/m.room.message/%x?access_token=%s",
					x.Server, room.RoomId, rand.Uint64(), x.AccessToken,
				)
				payload, err := json.Marshal(map[string]interface{}{
					"body":    event.formatted(),
					"msgtype": "m.text",
				})
				if err != nil {
					return err
				}

				req, err := http.NewRequest(http.MethodPut, url, bytes.NewBuffer(payload))
				req.Header.Set("Content-Type", "application/json")
				if err != nil {
					return err
				}

				go func() {
					resc := make(chan *http.Response)
					go func() {
						res, err := x.client.Do(req)
						if err != nil {
							x.errc <- err
							return
						}
						resc <- res
					}()

					select {
					case <-ctx.Done():
						return
					case res := <-resc:
						defer res.Body.Close()
						if res.StatusCode != 200 {
							x.errc <- errors.New("request failed")
						}
					}
				}()
			}
		}
	}
}
