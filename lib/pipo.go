package lib

import (
	"context"
	"fmt"
	"log"
	"time"
)

type pipoEvent struct {
	room    room
	from    string
	via     string
	on      string
	message string
}

func (x pipoEvent) formattedFrom() string {
	y := fmt.Sprintf("%s on %s", x.from, x.on)
	if len(x.via) > 0 {
		y = fmt.Sprintf("%s via %s on %s", x.from, x.via, x.on)
	}
	return y
}

func (x pipoEvent) formatted() string {
	return fmt.Sprintf("🤖[%s]: %s", x.formattedFrom(), x.message)
}

type Pipo struct {
	Closed   chan struct{}
	recv     chan pipoEvent
	services *map[string]service
}

func NewPipo(s *map[string]service) *Pipo {
	for k, v := range *s {
		v.init(k)
	}
	return &Pipo{
		Closed:   make(chan struct{}),
		recv:     make(chan pipoEvent),
		services: s,
	}
}

type pipoCtxT int

const (
	pipoRecv pipoCtxT = iota
	pipoTest
)

func ctxPipoRecv(ctx context.Context) chan pipoEvent {
	return ctx.Value(pipoRecv).(chan pipoEvent)
}

func (x *Pipo) Run(ctx context.Context) {
	defer func() { x.Closed <- struct{}{} }()

	ctx = context.WithValue(ctx, pipoRecv, x.recv)

	for _, s := range *x.services {
		go s.run(ctx)
		defer func(s service) { <-s.fin() }(s)
	}

	for {
		log.Printf("pipo")

		select {
		case <-ctx.Done():
			return

		case <-time.After(60 * time.Second):

		case event := <-x.recv:
			for _, s := range *x.services {
				go func(ctx context.Context, s service) {
					select {
					case <-ctx.Done():
						return
					case s.recv() <- event:
					}
				}(ctx, s)
			}
		}
	}
}
