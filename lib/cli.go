package lib

import "flag"

var FlagAddrSock = flag.String("sock", "127.0.0.1:5292", "socket address")

// pipo
var FlagCfgPath = flag.String("cfg", "./pipo.toml", "config path")
var FlagLogPath = flag.String("log", "./log.txt", "log path")

// raz
var FlagAddrHttp = flag.String("http", "0.0.0.0:5001", "address")
var FlagToken = flag.String("token", "", "token")

