package lib

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/slack-go/slack"
	"github.com/slack-go/slack/slackevents"
)

type slackChannel struct {
	ChannelId string `json:"channel_id" toml:"channel_id"`
	Group     string

	service *slackService
}

func (x *slackChannel) id() string {
	return x.service.id + "_" + x.ChannelId
}

func (x *slackChannel) group() string {
	return x.Group
}

func (x *slackChannel) sync(ctx context.Context) {
	c := pipoDictRow{Key: x.ChannelId + ":name"}
	go c.sync(ctx, func() (string, error) {
		c, err := x.service.client.GetConversationInfoContext(ctx,
			&slack.GetConversationInfoInput{ChannelID: x.ChannelId})
		if err != nil {
			return "", err
		}
		return c.Name, nil
	})
}

func (x *slackChannel) name(ctx context.Context) string {
	r := &pipoDictRow{Key: x.ChannelId + ":name"}
	r.get(ctx)
	return r.Value
}

type slackService struct {
	SenderId      string `json:"sender_id" toml:"sender_id"`
	SigningSecret string `json:"signing_secret" toml:"signing_secret"`
	AuthToken     string `json:"auth_token" toml:"auth_token"`
	Port          string
	Channels      []*slackChannel

	id    string
	finc  chan struct{}
	recvc chan pipoEvent

	router *chi.Mux
	server *http.Server
	client *slack.Client
	errc   chan error
}

func (x *slackService) recv() chan<- pipoEvent {
	return x.recvc
}

func (x *slackService) fin() <-chan struct{} {
	return x.finc
}

func (x *slackService) init(id string) {
	x.id = id
	x.finc = make(chan struct{})
	x.recvc = make(chan pipoEvent)
	x.errc = make(chan error)
	for _, channel := range x.Channels {
		channel.service = x
	}
}

func (x *slackService) sync(ctx context.Context) {
	t := pipoDictRow{Key: x.SenderId + ":name"}
	go t.sync(ctx, func() (string, error) {
		team, err := x.client.GetTeamInfoContext(ctx)
		if err != nil {
			return "", err
		}
		return team.Name, err
	})
}

func (x *slackService) workspaceName(ctx context.Context) string {
	r := &pipoDictRow{Key: x.SenderId + ":name"}
	r.get(ctx)
	return r.Value
}

func (x *slackService) run(ctx context.Context) {
	defer func() { x.finc <- struct{}{} }()

	x.router = chi.NewRouter()
	x.router.Post("/", func(w http.ResponseWriter, r *http.Request) {
		b, err := io.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		sv, err := slack.NewSecretsVerifier(r.Header, x.SigningSecret)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		if _, err := sv.Write(b); err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		if err := sv.Ensure(); err != nil {
			w.WriteHeader(http.StatusUnauthorized)
			return
		}

		eventsAPIEvent, err := slackevents.ParseEvent(
			json.RawMessage(b),
			slackevents.OptionNoVerifyToken(),
		)
		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		switch {
		case eventsAPIEvent.Type == slackevents.URLVerification:
			var r *slackevents.ChallengeResponse
			err := json.Unmarshal([]byte(b), &r)
			if err != nil {
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			w.Header().Set("Content-Type", "text")
			w.Write([]byte(r.Challenge))

		case eventsAPIEvent.Type == slackevents.CallbackEvent && eventsAPIEvent.InnerEvent.Type == "message":
			ev := eventsAPIEvent.InnerEvent.Data.(*slackevents.MessageEvent)
			if ev.BotID == x.SenderId {
				break
			}
			log.Printf("info %s %s", x.id, ev.Text)

			for _, c := range x.Channels {
				if c.ChannelId != ev.Channel {
					continue
				}

				// todo cache usernames
				var from string
				user, err := x.client.GetUserProfileContext(ctx,
					&slack.GetUserProfileParameters{UserID: ev.User})
				if err != nil {
					log.Printf("info %s warning %v", x.id, err)
					from = ev.User
				} else {
					from = user.RealName
				}

				ctxPipoRecv(ctx) <- pipoEvent{
					room:    c,
					from:    from,
					via:     c.name(ctx),
					on:      x.workspaceName(ctx) + " (Slack)",
					message: ev.Text,
				}
			}
		}
	})

	log.Printf("info starting %s", x.id)
	for err := x.runErr(ctx); err != nil; {
		log.Printf("info %s retrying in %d seconds %v", x.id, int(retryTimeout.Seconds()), err)
		<-time.After(retryTimeout)
	}
}

func (x *slackService) runErr(ctx context.Context) error {
	x.client = slack.New(x.AuthToken)
	go x.sync(ctx)
	for _, c := range x.Channels {
		c.sync(ctx)
	}

	x.server = &http.Server{
		Addr:    fmt.Sprintf(":%s", x.Port),
		Handler: x.router,
	}

	listenErr := make(chan error, 1)
	go func() {
		listenErr <- x.server.ListenAndServe()
	}()
	defer func() {
		if err := x.server.Shutdown(ctx); err != nil {
			log.Print(err)
		}
		<-listenErr
	}()

	for {
		select {
		case <-ctx.Done():
			return nil

		case err := <-x.errc:
			return err

		case err := <-listenErr:
			listenErr <- err
			return err

		case event := <-x.recvc:
			for _, c := range x.Channels {
				if c.id() == event.room.id() {
					continue
				}
				if c.Group != event.room.group() {
					continue
				}
				if _, _, err := x.client.PostMessage(
					c.ChannelId,
					slack.MsgOptionUsername(event.formattedFrom()),
					slack.MsgOptionText(event.message, false),
				); err != nil {
					return err
				}
			}
		}
	}
}
