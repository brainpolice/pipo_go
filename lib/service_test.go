package lib

import (
	"os"
	"testing"
)

func TestRes(t *testing.T) {
	cfgPath, err := ResolveCfgPath()
	if err != nil {
		t.Fatal(err)
	}
	t.Log(cfgPath)

	cfgBytes, err := os.ReadFile(cfgPath)
	if err != nil {
		t.Fatal(err)
	}

	s, err := ParseCfg(cfgBytes)
	if err != nil {
		t.Fatal(err)
	}

	t.Logf("%+v", s)
}

func TestToml(t *testing.T) {
	services, err := ParseCfg([]byte(`
[irc_chatjunkies]
service_type = "irc"
server = "irc.chatjunkies.org"
port = "6697"
nick = "joe_schmo_420"
channels = [
  { group = "one", name = "#redbook" },
  { group = "two", name = "#gigachads" }
]

[irc_freenode]
service_type = "irc"
server = "irc.freenode.net"
port = "6697"
nick = "joe_schmo_420"
channels = [
  { group = "one", name = "#redbook" },
  { group = "two", name = "#gigachads" }
]

["matrix_matrix.goettsch.xyz"]
service_type = "matrix"
sender = "pipo_go"
server = "matrix.goettsch.xyz"
access_token = "xxx"
port = "5000"
rooms = [
  { group = "one", room_id = "!gfulekzRaAyKmlLYCk:matrix.goettsch.xyz" },
  { group = "two", room_id = "!pjLqrdFwrnboaGtGIq:matrix.goettsch.xyz" }
]

[discord0]
service_type = "discord"
auth_token = "yyy"
channels = [
  { group = "one", channel_id = "1212221787056898058" },
  { group = "two", channel_id = "1071757763119689818" }
]

[slack0]
service_type = "slack"
sender_id = "B06N6V4NKQQ"
signing_secret = "zzz"
auth_token = "qqq"
port = "8009"
channels = [
  { group = "one", channel_id = "C06MLBF5SE7" },
  { group = "two", channel_id = "CS1Q8KBUN" }
]
	`))
	if err != nil {
		t.Fatal(err)
	}

	s := *services
	irc1 := s["irc_chatjunkies"].(*ircService)

	for _, ch := range irc1.Channels {
		t.Log(ch.id())
	}

	// {
	// 	c := irc1.channels[1581394266]
	// 	t.Log(c)
	// 	if c.group() != "one" {
	// 		t.Fatal()
	// 	}
	// }

	// {
	// 	c := irc1.channels[1688468575]
	// 	t.Log(c)
	// 	if c.group() != "two" {
	// 		t.Fatal()
	// 	}
	// }
}
