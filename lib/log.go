package lib

import (
	"fmt"
	"net"
	"time"
)

type SockLogger struct{}

var sockDialer = net.Dialer{Timeout: 3 * time.Second}

func (O SockLogger) Write(p []byte) (n int, err error) {
	conn, err := sockDialer.Dial("tcp", *FlagAddrSock)
	if err != nil {
		fmt.Println(err)
		return
	}

	defer func() {
		if err := conn.Close(); err != nil {
			fmt.Println(err)
		}
	}()

	n, err = conn.Write(p)
	if err != nil {
		fmt.Println(err)
	}

	return
}
