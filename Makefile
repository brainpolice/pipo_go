.PHONY: raz pipo sql

raz:
	GOOS=linux GOARCH=amd64 go build -o ./bin/raz ./cmd/raz/main.go

pipo:
	GOOS=linux GOARCH=amd64 go build -o ./bin/pipo ./cmd/pipo/main.go

sql:
	sqlite3 ./db.sqlite < ./sql/sqlite.sql
